<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <title>Tambah Negara</title> 
    <style> 
        label { 
            display: inline-block; 
            width: 150px; 
            padding-bottom: 10px;
        } 
    </style> 
</head> 
<body> 
    <h1>Tambah Negara</h1> 
    <form action="<?php echo site_url('country/prosestambah'); ?>" method="post"> 

        <label>Code Negara (3 Huruf)</label><input type="text" name="code"><br> 

       
        <label>Nama Negara</label><input type="text" name="name"><br> 

        <label>Benua</label><Select name="continent"> 
            <?php 
            foreach ($country->result() as $ctr) { 
             echo '<option value="'.$ctr->Continent.'">'.$ctr->Continent.'</option>'; 
            } 
            ?></Select><br> 
        
        <label>Wilayah</label><Select name="region"> 
            <?php 
            foreach ($country->result() as $ctr) { 
             echo '<option value="'.$ctr->Region.'">'.$ctr->Region.'</option>'; 
            } 
            ?></Select><br> 

        <label>Luas Wilayah</label><input type="number" name="surfacearea"><br> 

        <label>Hari Kemerdekaan</label><input type="number" name="indepyear"><br> 
        <label>Population</label><input type="number" name="population"><br> 
        <label>Life Expectancy</label><input type="number" name="lifeexpectancy"><br> 
        <label>GNP</label><input type="number" name="gnp"><br> 
        <label>GNP Old</label><input type="number" name="gnpold"><br> 

        <label>Local Name</label><input type="text" name="localname"><br> 
        <label>Government Form</label><input type="text" name="governmentform"><br> 
        <label>Head Of State</label><input type="text" name="headofstate"><br> 
        <label>Capital</label><input type="number" name="capital"><br> 
         <label>Code 2</label><Select name="code2"> 
            <?php 
            foreach ($country->result() as $ctr) { 
             echo '<option value="'.$ctr->Code2.'">'.$ctr->Code2.'</option>'; 
            } 
            ?></Select><br> 
        
        <input type="submit" value="Tambah"> 
    </form> 
</body> 
</html>
