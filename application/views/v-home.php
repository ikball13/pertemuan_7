<!DOCTYPE html>
<html lang="en">
<head>
	<title>Home</title>
	<link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.css')?>" rel="stylesheet">
</head>
<body>
	<div class="navbar navbar-expand bg-dark navbar-dark">
		<a class="navbar-brand" href="#">UNJANI</a>
		<ul class="navbar-nav">

			<li class="nav-item">
				<a class="nav-link" href="#">Home</a>
			</li>

			<li class="nav-item">
				<a class="nav-link" href="#">Profil</a>
			</li>

			<li class="nav-item">
				<a class="nav-link" href="#">Kalkulator</a>
			</li>
			<li>
				<a class="nav-link" href="<?= base_url('home/logout')?>"> Log Out</a>
			</li>
		</ul>
	</div>
	<br>
	<div class="container-fluid">
		<div class="jumbotron text-center">
			Welcome <?= $this->session->userdata('username'); ?>!
			<br>
			<img src="<?= $this->session->userdata('url'); ?>" class="rounded-circle w-25">
			
		</div>
	</div>
</body>
</html>
